public static void bubbleSort(int[] a, int n) {  
    // 外层循环控制排序趟数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环控制每一趟排序多少次  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end

