/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    for(int i = 0; i < n; i++) {
	    for(int b = 0; b < n - i - 1; b++) {
		    if(a[b] > a[b + 1]) {
			    int temp = a[b];
			    a [b] = a[b + 1];
			    a[b + 1] = temp;
		    }
	    }
    }


} //end
