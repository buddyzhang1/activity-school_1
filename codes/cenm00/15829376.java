/**  
 * 冒泡排序函数  
 * 对数组a进行冒泡排序，将其变为有序数组  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制比较的轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，控制每轮比较的次数  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
