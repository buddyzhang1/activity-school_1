/**  
 * 冒泡排序函数  
 * 将数组a从小到大进行排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制需要排序的趟数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每一趟排序多少次  
            if (a[j] > a[j + 1]) { // 如果前面的数比后面的数大，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
