/**  
 * 冒泡排序函数  
 * 通过对相邻的元素进行两两比较，顺序相反则进行交换，每一轮比较会将当前未排序部分的最大值“冒泡”到未排序部分的末尾。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制所有轮数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每轮比较的次数  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // endi
