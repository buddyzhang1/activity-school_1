/**  
 * 冒泡排序函数  
 * 通过不断比较相邻元素的大小，将较大的元素逐步“浮”到数组的末尾，从而实现排序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环，控制比较轮数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环，负责相邻元素间的比较和交换  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
