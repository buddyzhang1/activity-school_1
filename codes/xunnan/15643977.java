/**  
 * 冒泡排序函数  
 * 通过多次遍历数组，相邻元素两两比较并交换，使得较大的元素逐渐"浮"到数组的末尾，  
 * 从而实现数组的升序排序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 用于标记数组在本次遍历中是否发生了交换  
        boolean swapped = false;  
          
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记发生了交换  
                swapped = true;  
            }  
        }  
          
        // 如果本次遍历中没有发生交换，说明数组已经有序，无需继续遍历  
        if (!swapped) {  
            break;  
        }  
    }  
} //end

