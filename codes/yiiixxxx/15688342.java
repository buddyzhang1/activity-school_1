/**  
 * 冒泡排序函数  
 * 依次比较相邻的元素，如果顺序错误则交换它们，直到没有需要交换的元素为止。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标记是否有交换，用于优化在某一趟排序中无交换时提前结束  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记本趟有交换发生  
                swapped = true;  
            }  
        }  
        // 如果某一趟排序中没有发生交换，说明数组已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} // end
