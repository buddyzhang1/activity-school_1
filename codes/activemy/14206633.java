/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有
    if (0 >= n)
    {
        return;
    }
    int tmp,i,j;
    for (j=n-1; j>0; j--)
    {
        for (i=0; i<j; i++)
	{
	    if(a[i] > a[i+1])
	    {
	    	tmp = a[i+1];
		a[i+1] = a[i];
		a[i] = tmp;
	    }
	}
    }
    return;

} //end
