/**
 * 冒泡排序函数
 * 将数组a从小到大进行排序
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    for (int i = 0; i < n; i++) {
       for (int j = 0; j < n - i -1; j++) {
           if(a[j] > a[j+1]){
   		int temp = a[j];
		a[j] = a[j+1];
		a[j+1] = temp;
	   }
       }
    }
}    

