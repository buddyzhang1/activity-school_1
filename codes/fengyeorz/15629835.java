/**
 * 冒泡排序函数
 * 该函数实现了冒泡排序算法，将数组a中的元素按升序排列
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 遍历所有数组元素
    for (int i = 0; i < n - 1; i++) {
        // 标记变量，用于检查是否发生了交换
        boolean swapped = false;
        for (int j = 0; j < n - i - 1; j++) {
            // 如果当前元素大于下一个元素，则交换它们
            if (a[j] > a[j + 1]) {
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
                swapped = true;
            }
        }
        // 如果内层循环没有发生交换，说明数组已经有序，可以提前退出
        if (!swapped) {
            break;
        }
    }
} //end
