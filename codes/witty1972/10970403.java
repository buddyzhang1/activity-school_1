/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param l 待排序的数组长度
 */
public static void bubbleSort(int [] a, int l){
    // 你的代码，使无序数组 a 变得有序
    for (int i=0 ; i<l-1 ;i++){
        for (int k=0 ; k<l-1-i ;k++){
            if (a[k]>a[k+1]) {
                int temp=a[k];
                a[k]=a[k+1];
                a[k+1]=temp;
            }
       }
    }
} //end